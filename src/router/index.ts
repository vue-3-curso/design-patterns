import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HocView from '../views/HocView.vue'
import ContenedorVisualizadorView from '../views/ContenedorVisualizadorView.vue'
import ServiciosView from '../views/ServiciosView.vue'
import ProviderInjectView from '../views/ProviderInjectView.vue'
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'hoc',
    component: HocView
  },
  {
    path: '/contenedor-visualizador',
    name: 'contenedor-visualizador',
    component: ContenedorVisualizadorView
  },
  {
    path: '/servicios',
    name: 'servicios',
    component: ServiciosView
  },
  {
    path: '/provider-inject',
    name: 'provider-inject',
    component: ProviderInjectView
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router