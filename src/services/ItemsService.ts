import { IPost } from "../interfaces/IPost"

class ItemsService{

    private uri:string='https://jsonplaceholder.typicode.com/posts'

    async getItems(){
        const rawResponse=await fetch(this.uri)
        const response= await rawResponse.json()
        return response
    }
}

export default ItemsService